export interface Product {
  _id: String;
  name: String;
  codigo: String;
  price: Number;
  details: String;
  img: String;
  ofert: Boolean;
  desc: Number;
  cantidad: Number;
}
